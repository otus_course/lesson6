package ru.rt;


public class BubbleSort extends Sorter{

    @Override
    public void sort() {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if(arr[j]>arr[j+1]){
                    swap(j,j+1);
                }
            }
        }
    }

}
