package ru.rt;

public class ShellSort extends Sorter{

    @Override
    void sort() {
        sort2();
    }

    void sort1(){
        for (int gap = arr.length/2; gap > 0; gap/=2) {
            internalArray(gap);
        }
    }

    //A003462
    void sort2(){
        for (int gap = arr.length/3; gap > 0; gap=gap/3) {
            internalArray(gap);
        }
    }

    //A036562
    void sort3(){
        for (int i = 15; i >= 0 ; i--) {
            int gap = 1;
            if(i > 0) gap = (int) Math.pow(4, i) + 3 * (int) Math.pow(2, i - 1) + 1;

            if(gap < arr.length) internalArray(gap);
        }
    }

    private void internalArray(int gap){
        for (int i = 0; i + gap < arr.length; i++) {
            int j = i+gap;
            int tmp = arr[j];
            while (j-gap >=0 && arr[j-gap] > tmp){
                arr[j] = arr[j-gap];
                j-=gap;
            }
            arr[j]=tmp;
        }
    }
}
