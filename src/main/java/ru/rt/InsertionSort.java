package ru.rt;

public class InsertionSort extends Sorter{
    @Override
    void sort() {
        int i = 1;
        while(i < arr.length){
            int j = i;
            while (j>0 && arr[j-1] > arr[j]){
                swap(j,j-1);
                j--;
            }
            i++;
        }
    }
}
