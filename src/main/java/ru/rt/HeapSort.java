package ru.rt;

public class HeapSort extends Sorter{
    @Override
    void sort() {
        for (int root = arr.length/2-1; root >= 0 ; root--) {
            moveMaxToRoot(root,arr.length);
        }

        for (int i = arr.length - 1; i >=0 ; i--) {
            swap(0,i);
            moveMaxToRoot(0,i);
        }
    }

    void moveMaxToRoot(int root, int size){
        int l = 2*root + 1;
        int r = 2*root + 2;

        int x = root;

        if(l < size && arr[x] < arr[l]) x = l;
        if(r < size && arr[x] < arr[r]) x = r;

        if(x == root) return;

        swap(x,root);
        moveMaxToRoot(x,size);

    }
}
