package ru.rt;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class Sorter implements Task{

    protected int size;
    protected int[] arr;

    @Override
    public String run(String[] data) {
        this.size = Integer.parseInt(data[0]);
        String str = data[1];
        this. arr = Stream.of(str.split(" ")).mapToInt(x->Integer.parseInt(x)).toArray();

        long start = System.currentTimeMillis();
        sort();
        System.out.println(this.getClass().getSimpleName()+" size: " + size + " time: " +(System.currentTimeMillis() - start) + " ms.");
        return Arrays.stream(arr).mapToObj(String::valueOf).collect(Collectors.joining(" "));
    }

    abstract void sort();

    public void swap(int i, int j){
        int x = arr[i];
        arr[i] = arr[j];
        arr[j] = x;
    }
}
